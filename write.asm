.386
.model flat,stdcall
includelib c:\masm32\lib\kernel32.lib
include c:\masm32\include\kernel32.inc

.data
sConsoleTitle db 'Count chars',0
sReadText db 128 dup('q')
countZ db 0
countSpace db 0
n dd ?
hStdin dd ?
hStdout dd ?

.code
Start PROC
    invoke SetConsoleTitle, offset sConsoleTitle

    invoke GetStdHandle, -10
    mov hStdin,eax

    invoke ReadConsole, hStdin, offset sReadText, 20, offset n, 0

    mov ecx,5
    mov eax, offset sReadText
    CountSpacesAndZ:
        .if eax == 'z'
            inc countZ
        .endif
        .if eax == ' '
            inc countSpace
        .endif
        inc eax
    loop CountSpacesAndZ

    mov ebx,n
    invoke GetStdHandle, -11
    mov hStdOut,eax
    invoke WriteConsole, hStdout, offset sReadText, ebx, 0, 0
    invoke WriteConsole, hStdout, offset countZ, ebx, 0, 0
    invoke WriteConsole, hStdout, offset countSpace, ebx, 0, 0

    invoke Sleep, 2000d
    invoke ExitProcess, 0
Start ENDP
end Start